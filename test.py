import os
import time
import logging
import mechanicalsoup
import validators

from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
import selenium.webdriver as webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import pickle

# Program startup
#API_TOKEN = '678958468:AAE9G0J-jRrNfyowHWeS0kAv7uPw1H8KK7o'
API_TOKEN = '669377134:AAF94-KDjQ9DgSepc0TngEs7GxIuRSw9LRk'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
# For example use simple MemoryStorage for Dispatcher.
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
browser = mechanicalsoup.StatefulBrowser(user_agent='MechanicalSoup')
link_login = "https://www.storyblocks.com/login"
link_verify = "https://www.storyblocks.com/verify"
profile = webdriver.FirefoxProfile("/home/daulet/PycharmProjects/scraper/epgb7st9.default-release")


# States
class Form(StatesGroup):
    code = State()  # Will be represented in storage as 'Form:code'
    link = State()  # Will be represented in storage as 'Form:link'


class Browser(StatesGroup):
    browser = State()


@dp.message_handler(commands='start1')
async def cmd_start(message: types.Message, state: FSMContext):
    """
    Conversation's entry point
    """
    # Set state
    await Form.code.set()

    firefox = webdriver.Firefox(profile)
    firefox.get(link_login)

    login_form = firefox.find_element_by_id("login-form")
    login_form.find_element_by_id("email").send_keys('blabla@gmail.com')
    login_form.find_element_by_id("password").send_keys('123123123')
    login_form.submit()

    WebDriverWait(firefox, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.verifyCode-submitButton")))

    firefox.get(link_verify)
    firefox.find_element_by_class_name('verifyCode-submitButton').find_element_by_class_name('hide-on-verify').click()
    pickle.dump(firefox.get_cookies(), open("cookies.pkl", "wb"))

    await message.reply("Привет! Скинь код верификации")


# You can use state '*' if you need to handle all states
@dp.message_handler(state='*', commands='cancel')
@dp.message_handler(Text(equals='cancel', ignore_case=True), state='*')
async def cancel_handler(message: types.Message, state: FSMContext):
    """
    Allow user to cancel any action
    """
    current_state = await state.get_state()
    if current_state is None:
        return

    logging.info('Cancelling state %r', current_state)
    # Cancel state and inform user about it
    await state.finish()
    # And remove keyboard (just in case)
    await message.reply('Cancelled.', reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Form.code)
async def process_code(message: types.Message, state: FSMContext):
    firefox = webdriver.Firefox(profile)
    firefox.get(link_verify)

    cookies = pickle.load(open("cookies.pkl", "rb"))
    for cookie in cookies:
        firefox.add_cookie(cookie)

    form = firefox.find_element_by_id('#send-code-form')
    form.find_element_by_id('verification_code').send_keys(message.text)
    form.find_element_by_id('verify-code-button').click()

    """
    Process user verification code
    """
    async with state.proxy() as data:
        data['code'] = message.text

    await Form.next()
    await message.reply("Отправь ссылку в Storyblocks.com")


@dp.message_handler(content_types=['text'])
async def get_video(message: types.Message):
    if not validators.url(message.text):
        await bot.send_message(message.chat.id, 'Отправьте рабочую ссылку')

    profile.set_preference("browser.download.manager.showWhenStarting", False)
    profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                           "video/mp4;video/x-sgi-movie;application/mp4;video/quicktime;video/x-msvideo;"
                           + "application/x-rar-compressed;"
                           + "application/zip;application/octet-stream;application/x-zip-compressed;multipart/x-zip")
    firefox = webdriver.Firefox(profile)
    firefox.get(message.text)

    print('webpage is ready, preparing to click')
    WebDriverWait(firefox, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button.PrimaryButton")))
    firefox.find_element_by_class_name('memberDownloadCta').find_element_by_class_name('PrimaryButton').click()

    print('sleeping 5 sec')
    time.sleep(5)

    split = message.text.split('/')

    prefix = split[-1]
    split = prefix.split('-')
    split.pop()
    prefix = '-'.join(split)
    downloads_path = '/home/daulet/Загрузки/'
    file = ''

    for i in os.listdir(downloads_path):
        if os.path.isfile(os.path.join(downloads_path, i)) and prefix in i:
            file = i

    if file == '':
        prefix = "videoblocks-" + prefix
        for i in os.listdir(downloads_path):
            if os.path.isfile(os.path.join(downloads_path, i)) and prefix in i:
                file = i

    f = open(downloads_path + file, "rb")
    b = os.path.getsize(downloads_path + file)

    if b > 52428800:
        await bot.send_message(message.chat.id, 'Размер файла слишком велик, не могу отправить.')
    else:
        await bot.send_document(message.chat.id, f.read())


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
