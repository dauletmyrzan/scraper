#!/usr/bin/env python
__author__ = "Daulet Myrzan @dauletmyrzan"
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Daulet Myrzan"
__email__ = "dauletmyrzan@gmail.com"
__status__ = "Production"

import os
import datetime
import pathlib
import time
import logging
import mechanicalsoup
import validators
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import State, StatesGroup
import selenium.webdriver as webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from urllib.parse import urlparse

# Program startup
API_TOKEN = '1645729129:AAHS5T18FFpdw0Ir2W2t1lHYOEBseMCWyAE'
#API_TOKEN = '669377134:AAFlSGYagWCjhT4LDmr1dO1JOHdjyiEZIw4'
allowed = []

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
# For example use simple MemoryStorage for Dispatcher.
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
browser = mechanicalsoup.StatefulBrowser(user_agent='MechanicalSoup')
link_login = "https://www.storyblocks.com/login"
link_verify = "https://www.storyblocks.com/verify"


def uri_validator(x):
    try:
        result = urlparse(x)
        return all([result.scheme, result.netloc, result.path])
    except:
        return False


# States
class Form(StatesGroup):
    code = State()  # Will be represented in storage as 'Form:code'
    email = State()  # Will be represented in storage as 'Form:email'


critical_function_lock = Lock()


@dp.message_handler(commands=['start'])
async def start(message: types.Message):
    if message.chat.id not in allowed:
        split = message.text.split(' ')

        if split[-1] != 'bXlyemFu':
            await bot.send_message(message.chat.id, 'Извините, по вашей ссылке доступ запрещен. Введите пароль')
        else:
            await bot.send_message(message.chat.id, 'Отправьте ссылку на ролик, который хотите скачать')

            if message.chat.id not in allowed:
                allowed.append(message.chat.id)
    else:
        await bot.send_message(message.chat.id, 'Отправьте ссылку на ролик, который хотите скачать')


@dp.message_handler(content_types=['text'])
async def get_video(message: types.Message):
    id = str(message.chat.id)
    if message.text == 'bXlyemFu' and message.chat.id not in allowed:
        allowed.append(message.chat.id)
        await bot.send_message(message.chat.id, 'Отправьте ссылку на ролик, который хотите скачать')
        return
    elif message.chat.id not in allowed:
        await bot.send_message(message.chat.id, 'Извините, по вашей ссылке доступ запрещен. Введите пароль')
        return
    elif not uri_validator(message.text):
        await bot.send_message(message.chat.id, 'Отправьте ссылку из storyblocks.com')
        return

    if critical_function_lock.locked():
        await bot.send_message(message.chat.id, 'Бот уже обрабатывает другие видео, пожалуйста подождите.')
        return

    critical_function_lock.acquire()

    try:
        profile = webdriver.FirefoxProfile("epgb7st9.default-release")
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                               "video/mp4;video/x-sgi-movie;application/mp4;video/quicktime;video/x-msvideo;"
                               + "application/x-rar-compressed;"
                               + "application/zip;application/octet-stream;application/x-zip-compressed;multipart/x-zip")

        await bot.send_message(message.chat.id, 'Ссылка обрабатывается. Подождите 2-5 минут. Пожалуйста, чтобы я не сломался, не отправляйте другие ссылки, пока текущая не обработается. Спасибо!')

        firefox_options = webdriver.FirefoxOptions()
        firefox_options.add_argument("-headless")

        firefox = webdriver.Firefox(firefox_profile=profile, options=firefox_options, executable_path='/usr/local/bin/geckodriver')
        print(id + ' Firefox initialized')
        profile.set_preference("webdriver.load.strategy", "unstable")
        firefox.set_page_load_timeout(10)

        print(id + ' Trying to get url...')
        try:
            firefox.get(message.text)
        except TimeoutException:
            firefox.execute_script("window.stop();")
        print(id + ' Url loaded')

        time.sleep(2)

        print(id + ' webpage is ready, preparing to click')
        WebDriverWait(firefox, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button.PrimaryButton")))

        try:
            firefox.find_element_by_id('HDMP4').click()
            time.sleep(1)
        except:
            print(id + ' element #HDMP4 not found')

        firefox.find_element_by_class_name('memberDownloadCta').find_element_by_class_name('PrimaryButton').click()
        now = datetime.datetime.now()

        print(id + ' sleeping 10 sec')
        time.sleep(10)

        split = message.text.split('/')

        prefix = split[-1]
        split = prefix.split('-')
        split.pop()
        prefix = '-'.join(split)
        downloads_path = '/root/Загрузки/'
        file = ''

        print(id + ' Getting through /root/Загрузки/ using prefix "' + prefix + '"')

        for i in os.listdir(downloads_path):
            if os.path.isfile(os.path.join(downloads_path, i)) and prefix in i:
                file = i
                break

        if file == '':
            print(id + ' Could not find file!')
            prefix = "videoblocks-" + prefix
            print(id + ' Getting through /root/Загрузки/ using prefix "' + prefix + '"')
            for i in os.listdir(downloads_path):
                if os.path.isfile(os.path.join(downloads_path, i)) and prefix in i:
                    file = i
                    break

        if file == '':
            print(id + ' Could not find file!')
            print(id + ' Getting through /root/Downloads/ using file created time')
            for i in os.listdir(downloads_path):
                if os.path.isfile(os.path.join(downloads_path, i)):
                    fname = pathlib.Path(os.path.join(downloads_path, i))
                    ctime = datetime.datetime.fromtimestamp(fname.stat().st_ctime)
                    duration = now - ctime
                    duration_in_sec = duration.total_seconds()
                    if duration_in_sec < 30:
                        file = i
                        break

        if file == '':
            print(id + ' Could not find file using datetime')
            await bot.send_message(message.chat.id, 'К сожалению, я не смог скачать видео по вашей ссылке')
            pass

        global filename
        filename = file
        f = open(downloads_path + file, "rb")
        b = os.path.getsize(downloads_path + file)
        global file_path
        file_path = downloads_path + file

        if b > 52428800:
            await bot.send_message(message.chat.id, 'Размер файла слишком велик, не могу отправить.')
            pass
        else:
            await bot.send_video(message.chat.id, f.read())
    except Exception as e:
        await bot.send_message(message.chat.id, 'Возникла ошибка. Возможно вы пытаетесь скачать файл другого формата. '
                               + 'Бот может скачать только mp4 файлы.\nСлужебное сообщение: ' + str(e))
        print(str(e))
    finally:
        print(id + ' releasing lock')
        critical_function_lock.release()
    if 'firefox' in locals():
        print(id + ' Closing Firefox')
        firefox.quit()


if __name__ == '__main__':
    executor.start_polling(dispatcher=dp, skip_updates=True)
