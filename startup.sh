#!/usr/bin/sh
cd /home

file="bot.pid"
PID=$(cat "$file")

if [[ "" != "$PID" ]]; then
  kill -9 $PID
fi

/usr/local/bin/python3.8 main.py & echo $! > bot.pid
